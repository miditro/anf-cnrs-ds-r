#+TITLE: Programme du 15/11/2021

** Première partie : session plénière (C. Halgand)
Introduction générale à la formation, commune aux deux parcours.

*** Présentation
- Déroulé pédagogique de la formation
- Info sur les données et le contexte scientifique, objectifs de l'étude
- Organisation des données

*** Plan participatif
Réfléchir collectivement aux étapes du plan d'analyse statistique.

** Seconde partie : parcours R (F. Santos)
La formation se poursuit avec les stagiaires R uniquement, pour une introduction aux outils qui seront employés durant la formation.

- Vérification de l'installation des logiciels
- Utilisation de Git et GitLab
- Création et configuration de votre dépôt personnel sur GitLab pour la formation
- "Tour de bienvenue" de JupyterLab, présentation des plugins qui seront utilisés
- Introduction à la programmation lettrée ; rédaction d'un premier notebook Jupyter

Pour une présentation alternative de Git et Jupyter, voir [[https://gitub.u-bordeaux.fr/python_r/ANF_CNRS_Data_Science_Parcours_Python/-/tree/main/jour1/Les_Outils/Git_GitHub_GitLab][les documents pédagogiques par le formateur du parcours Python]].
